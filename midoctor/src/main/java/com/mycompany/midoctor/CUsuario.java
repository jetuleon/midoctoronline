/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.midoctor;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author jesus
 */
public class CUsuario {
    int codigo;
    String nombresUsuario;
    String apellidosUsuario;
    String dniUsuario;
    String telefonoUsuario;
    String correoUsuario;
    String usuarioUsuario;
    String claveUsuario;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombresUsuario() {
        return nombresUsuario;
    }

    public void setNombresUsuario(String nombresUsuario) {
        this.nombresUsuario = nombresUsuario;
    }

    public String getApellidosUsuario() {
        return apellidosUsuario;
    }

    public void setApellidosUsuario(String apellidosUsuario) {
        this.apellidosUsuario = apellidosUsuario;
    }

    public String getDniUsuario() {
        return dniUsuario;
    }

    public void setDniUsuario(String dniUsuario) {
        this.dniUsuario = dniUsuario;
    }

    public String getTelefonoUsuario() {
        return telefonoUsuario;
    }

    public void setTelefonoUsuario(String telefonoUsuario) {
        this.telefonoUsuario = telefonoUsuario;
    }

    public String getCorreoUsuario() {
        return correoUsuario;
    }

    public void setCorreoUsuario(String correoUsuario) {
        this.correoUsuario = correoUsuario;
    }

    public String getUsuarioUsuario() {
        return usuarioUsuario;
    }

    public void setUsuarioUsuario(String usuarioUsuario) {
        this.usuarioUsuario = usuarioUsuario;
    }

    public String getClaveUsuario() {
        return claveUsuario;
    }

    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }
    
    public void InsertarUsuario(JTextField paramNombres, JTextField paramApellidos,JTextField paramdni,JTextField paramTelefono,JTextField paramCorreo,JTextField paramUsuario,JTextField paramClave){
        
        setNombresUsuario(paramNombres.getText());
        setApellidosUsuario(paramApellidos.getText());
        setDniUsuario(paramdni.getText());
        setTelefonoUsuario(paramTelefono.getText());
        setCorreoUsuario(paramCorreo.getText());
        setUsuarioUsuario(paramUsuario.getText());
        setClaveUsuario(paramClave.getText());
        
        CConexion objetoConexion = new CConexion();
        
        String consulta = "insert into usuario (nombres,apellidos,dni,telefono,correo,nombre_usuario,contrasena_usuario) values(?,?,?,?,?,?,?);";
        
        try {
            CallableStatement cs = objetoConexion.estableceConexion().prepareCall(consulta);
            
            cs.setString(1, getNombresUsuario());
            cs.setString(2, getApellidosUsuario());
            cs.setString(3, getDniUsuario());
            cs.setString(4, getTelefonoUsuario());
            cs.setString(5, getCorreoUsuario());
            cs.setString(6, getUsuarioUsuario());
            cs.setString(7, getClaveUsuario());
            
            cs.execute();
            
            JOptionPane.showMessageDialog(null, "Se ingreso corectamente el Usuario");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "No see ingreso corectamente el Usuario, error:"+e.toString());
        }
    }
    
    public void MostrarUsuarios(JTable paramTablaTotalUsuarios){
        
        CConexion objetoConexion = new CConexion();
        DefaultTableModel modelo = new DefaultTableModel();
        
        TableRowSorter<TableModel> ordenarTabla = new TableRowSorter<TableModel>(modelo);
        paramTablaTotalUsuarios.setRowSorter(ordenarTabla);
        
        String sql = "";
        
        modelo.addColumn("Id");
        modelo.addColumn("Nombres");
        modelo.addColumn("Apellidos");
        modelo.addColumn("dni");
        modelo.addColumn("Telefono");
        modelo.addColumn("Correo");
        modelo.addColumn("Usuario");
        modelo.addColumn("Clave");
        
        paramTablaTotalUsuarios.setModel(modelo);
        
        sql= "select *from usuario; ";
        
        String[] datos = new String[8];
        Statement st;
        
        try {
            
            st= (Statement) objetoConexion.estableceConexion().createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                
                modelo.addRow(datos);
            }
            paramTablaTotalUsuarios.setModel(modelo);
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(null, "Nose pudo mostrar los Registros, error: "+e.toString());
        }
        
    }
    
    public void SelecionarUsuario(JTable paramTablaUsuarios, JTextField paramId, JTextField paramNombres, JTextField paramApellidos, JTextField paramdni, JTextField paramTelefono, JTextField paramCorreo, JTextField paramUsuario, JTextField paramClave){
        
        try {
            int fila = paramTablaUsuarios.getSelectedRow();
            
            if (fila>=0){
                paramId.setText(paramTablaUsuarios.getValueAt(fila, 0).toString());
                paramNombres.setText(paramTablaUsuarios.getValueAt(fila, 1).toString());
                paramApellidos.setText(paramTablaUsuarios.getValueAt(fila, 2).toString());
                paramdni.setText(paramTablaUsuarios.getValueAt(fila, 3).toString());
                paramTelefono.setText(paramTablaUsuarios.getValueAt(fila, 4).toString());
                paramCorreo.setText(paramTablaUsuarios.getValueAt(fila, 5).toString());
                paramUsuario.setText(paramTablaUsuarios.getValueAt(fila, 6).toString());
                paramClave.setText(paramTablaUsuarios.getValueAt(fila, 7).toString());
            }
            
            else{
                JOptionPane.showMessageDialog(null, "Fila no selecionada");
            }            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de seleccion, error: "+e.toString());
        }
    }
    
    public void ActualizarUsuarios(JTextField paramCodigo, JTextField paramNombres, JTextField paramApellidos, JTextField paramdni, JTextField paramTelefono, JTextField paramCorreo, JTextField paramUsuario, JTextField paramClave){
        
        setCodigo(Integer.parseInt(paramCodigo.getText()));
        setNombresUsuario(paramNombres.getText());
        setApellidosUsuario(paramApellidos.getText());
        setDniUsuario(paramdni.getText());
        setTelefonoUsuario(paramTelefono.getText());
        setCorreoUsuario(paramCorreo.getText());
        setUsuarioUsuario(paramUsuario.getText());
        setClaveUsuario(paramClave.getText());
        
        CConexion objetoConexion =new CConexion();
        
        String consulta = "UPDATE usuario SET usuario.nombres = ?,usuario.apellidos = ?, usuario.dni = ?, usuario.telefono = ?, usuario.correo = ?, usuario.nombre_usuario = ?, usuario.contrasena_usuario = ? WHERE usuario.id=?";
        
        try {
            
            CallableStatement cs = objetoConexion.estableceConexion().prepareCall(consulta);
            
            cs.setString(1, getNombresUsuario());
            cs.setString(2, getApellidosUsuario());
            cs.setString(3, getDniUsuario());
            cs.setString(4, getTelefonoUsuario());
            cs.setString(5, getCorreoUsuario());
            cs.setString(6, getUsuarioUsuario());
            cs.setString(7, getClaveUsuario());
            cs.setInt(8, getCodigo());
            
            cs.execute();
            
            JOptionPane.showMessageDialog(null, "Modificacion Exitosa");
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se modifico, error: "+e.toString());
        }
        }
    
    public void EliminarUsuarios(JTextField paramCodigo){
        
        setCodigo(Integer.parseInt(paramCodigo.getText()));
        
        CConexion objetoConexion = new CConexion();
        
        String consulta ="DELETE FROM usuario WHERE usuario.id=?";
        
        try {
            CallableStatement cs = objetoConexion.estableceConexion().prepareCall(consulta);
            cs.setInt(1, getCodigo());
            cs.execute();
            
            JOptionPane.showMessageDialog(null, "Se Elimino Correctamente el Usuario");
            
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se Pudo Eliminar Correctamente el Usuario, error: "+e.toString());
        }
    
    }
}
