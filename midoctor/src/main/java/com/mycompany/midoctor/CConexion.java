/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.midoctor;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author jesus
 */
public class CConexion {
    Connection conectar = null;
    
    String usuario = "root";
    String contrasena = "2023";
    String bd = "bddoctor";
    String servidor = "localhost";
    String puerto = "3307";       
    
    String cadena = "jdbc:mysql://"+servidor+":"+puerto+"/"+bd;
    
    public Connection estableceConexion(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conectar = DriverManager.getConnection(cadena,usuario,contrasena);
            //JOptionPane.showMessageDialog(null, "La conexion se a realizado con exito");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al conectarse a la Base de Datos, erro: "+e.toString());
        }
        return conectar;
    }
}
